//publisher.js
var jackrabbit = require('jackrabbit');

// Get the URL from ENV or default to localhost
var url = process.env.CLOUDAMQP_URL || "amqp://localhost";

// Connect to CloudAMQP
var rabbit = jackrabbit(url);
var exchange = rabbit.default();

var hello = exchange.queue({ name: 'example_queue', durable: true });

//publich message
exchange.publish({ msg: 'Hello from Harish K' }, { key: 'example_queue' });
exchange.on('drain', process.exit);